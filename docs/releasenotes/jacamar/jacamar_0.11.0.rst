Jacamar CI v0.11.0
==================

.. _conflicting parameters: ../../ci-users/ci-batch.html#conflicting-parameters

* *Release*: `v0.11.0 <https://gitlab.com/ecp-ci/jacamar-ci/-/releases/v0.11.0>`_
* *Date*: 4/29/2022

.. important::

    GitLab server *v15.0*
    `deprecates V1 of the CI_JOB_JWT <https://docs.gitlab.com/ee/update/deprecations#changes-to-the-ci_job_jwt>`_.
    This release aims to function with both V1 and V2, earlier
    versions of Jacamar CI will not worth with server releases after *v15.0*.

.. note::

    Release *v0.9.0* relocated all RPM installed binaries into a single
    location, ``/opt/jacamar/bin``. This will offer a better standard moving
    forward, please be aware of this when upgrading from an older version.

User Changes
------------

* Provide ``JACAMAR_CI_SHELL`` variable to Bash login
  (`!345 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/345>`_)

  - .. code-block:: bash

        # .bash_profile | .profile | .bashrc
        if [ ${JACAMAR_CI_SHELL} -eq 1 ] ; then
            # preform ci only profile operations...
        fi

* Conflicting scheduler parameters result in job failure
  (`!326 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/326>`_)

  - Previous iterations only warned of conflicting arguments with
    runner behaviours, any `conflicting parameters`_
    encountered will now result in job failures.

* Corrected scheduler file tailing
  (`!335 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/335>`_,
  `!340 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/340>`_)

  - This should address incorrect behaviours that lead to all output from
    supported schedulers to be held until the submitted job was completed.

Admin Changes
-------------

* Update JWT package and support both V1 and V2
  (`!342 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/342>`_)
* Conflicting scheduler parameters result in job failure
  (`!326 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/326>`_)

  * Enabled by default, can be disabled via configuration:

    .. code-block:: toml

        [batch]
        allow_illegal_args = false

* PBS executor more closely aligns with Cobalt and expanded testing
  (`!348 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/348>`_,
  `!346 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/346>`_)

  - At this time ``executor = "pbs"`` is still considered in development
    and testing/acceptance on supporting systems is still required.

Bug & Development Fixes
-----------------------

* Upgraded to Go version *1.18.1* and dependencies
  (`!338 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/338>`_)
* Removed un-supported federation options
  (`!337 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/337>`_)
* CI file organization and testing coverage expanded
  (`!332 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/332>`_,
  `!336 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/336>`_,
  `!347 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/347>`_)
