Jacamar CI v0.2.0
=================

* *Release*: `v0.2.0 <https://gitlab.com/ecp-ci/jacamar-ci/-/releases/v0.2.0>`_
* *Date*: 8/30/2020
* *Commit*: deed6cdb

The ``0.2.0`` release carriers with it a number of massive structural changes
that progress the drive towards a fully realized and stable production release.
The majority of these changes are not targeted at either our end CI users or
administrator. Instead they lay necessary groundwork for further development
efforts coupled with desirable security enhancements.

.. note::

  Please avoid production deployments of privileged processes relating
  to Jacamar until you've fully vetted the current version in a testing
  environment.

General Notes
-------------

* The concurrent number will now appear in the builds director zero
  padded to remain three characters long at all times.

  * For example, you project directory may appear like this:
    ``/ecp/user/builds/runner/001/group/project``

* Checking the ``--version`` will now properly display if uncommitted
  changes in the build via a percent (``%``) sign.

  .. code-block:: console

    $ jacamar --version
    Version: 0.2.0-pre.f57a3f1%
    Git Commit: f57a3f1%
    ...

Experimental Notes
------------------

.. important::

  Functionality added in an experimental state is subject to change and
  should not be relied upon in production workflows. It is added normally
  to facility further testing and feedback amongst users.

* You can now influence the Bash environment provided in which all your
  CI job scripts will run by defining one of two CI variables.

  * ``JACAMAR_NO_BASH_PROFILE: "true"`` - This will specify a ``--noprofile``
    for the shell, avoiding source any profiles (both system and user)
    prior to execution.

  * ``JACAMAR_ETC_PROFILE_ONLY: "true"`` - This will fundamentally
    ``source /etc/profile`` while leaving all user defined profiles
    un-sourced.

  * Default functionality was akin to ``bash --login`` which can lead to
    undesirable functionality when dealing with setuid and shared projects.

Admin Notes
-----------

* In the ``[auth]`` configuration section we have renamed ``run_mechanism``
  to ``downscope`` to better align with the setting functionality and
  underlying code enhancements.

  .. code-block:: toml

    [auth]
      enabled = true
      downscope = "setuid"

  * Please **update your Jacamar configuration** accordingly if you have
    not already done so.

* Jacamar will no longer ``chown`` scripts to the downscoped user, instead
  they are encoded by Jacamar-Auth and provided via environment variables.
  After which Jacamar will reconstruct a script locally, owned by the user.

  * Additional testing is required but we believe we have finally
    reached a point where *root* is no longer required.

* Spack packages have been created to support building a custom patched
  version of the GitLab runner with the minimal number of requirements
  to safely operate Jacamar.

  .. code-block:: console

    $ spack repo add $(pwd)/build/package/Spack/jacamar-repo
    ==> Added repo with namespace 'jacamar-repo'.
    $ spack install gitlab-runner@13.2.1+jacamar+federation
    ...
    $ spack load gitlab-runner@13.2.1+jacamar+federation
    $ gitlab-runner --version
    Version:      13.2.1
    ...

  * These packages are not ready for upstreaming at this time and are
    only meant to assist in deploying to your test environment. We aim
    to further diminish the patching requirement in the coming ``0.3.0``
    release.

* All job tokens are still hidden from ``/proc`` using approximately
  the same ways; however, the changes to the script and creation of
  the ``GIT_ASKPASS`` script are not managed by Jacamar directly.

  * Any issues you notice with jobs relating to Git/artifact behaviors
    should be reported to development.

* A high (100+) number of concurrent jobs should no longer lead to
  unexpected job failures due to the number of files open.

  * However, if all jobs are associated with a single user this
    may not be true in some very high concurrency circumstances.
