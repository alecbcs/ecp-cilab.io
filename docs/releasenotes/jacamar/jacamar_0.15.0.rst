Jacamar CI v0.15.0
==================

.. _Migrating to new id_tokens from CI_JOB_JWT: ../../guides/id-token-migration.html

* *Release*: `v0.15.0 <https://gitlab.com/ecp-ci/jacamar-ci/-/releases/v0.15.0>`_
* *Date*: 4/6/2023

Admin Changes
-------------

* Improve JWT handling in advance of server *v16.0*
  (`!427 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/427>`_)

  - Please see the `Migrating to new id_tokens from CI_JOB_JWT`_
    guide for complete details.

Bug & Development Fixes
-----------------------

* Removed support for ``ff_jwt_v2`` configuration
  (`!426 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/426>`_)
* Upgraded to Go version *1.19.8*
  (`!428 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/428>`_)
* Set version of vulnerability checker to correct json output
  (`!429 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/429>`_)
