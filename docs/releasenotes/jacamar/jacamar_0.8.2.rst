Jacamar CI v0.8.2
=================

* *Release*: `v0.8.2 <https://gitlab.com/ecp-ci/jacamar-ci/-/releases/v0.8.2>`_
* *Date*: 09/23/2021

Bug & Development Fixes
-----------------------

* Properly handle GitLab username during JWT validation
  (`!260 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/260>`_).

  - This addresses an issue where usernames with a period where incorrectly
    being labeled as invalid, causing job failures.

* Added backup mechanism to help identify successful job in Cobalt
  (`!252 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/252>`_).
