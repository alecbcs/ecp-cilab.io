Jacamar CI v0.14.0
==================

* *Release*: `v0.14.0 <https://gitlab.com/ecp-ci/jacamar-ci/-/releases/v0.14.0>`_
* *Date*: 2/16/2023

Admin Changes
-------------

* Option to allow LSF job termination via ``bkill``
  (`!417 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/417>`_,
  `!420 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/420>`_)
  - (Thanks ``@VarunKoyyalagunta``)

  - .. code-block:: toml

        [batch]
        lsf_job_cancellation = true

  - Enabling ``lsf_job_cancellation`` allows the LSF executor to use
    the ``bkill`` application to signal a running job it's time to stop. It
    also enforces that ``-J`` will be an illegal argument and as such
    may break existing jobs if they have previously relied upon setting this
    in their ``SCHEDULER_PARAMETER``.

* Added a newline to the signal help text for ``jacamar`` application
  (`!405 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/405>`_)
  - (Thanks ``@tiggi``)

* Improved message handling for RunAs validation
  (`!421 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/421>`_)

  - Expanded the RunAs JSON override to allow for admin defined
    ``user_message`` and ``error_message`` strings. If present
    these new details will be handled accordingly.

* Feature flag to allow expanded testing for limited build directories
  (`!378 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/378>`_)

  - .. important::

        This feature is still subject to modification and should be reserved
        to extended periods of testing/review. At this time as we have not
        gathered sufficient feedback at scale to remove the feature flag.

  - .. code-block:: toml

        [general]
        # Allow for a limited structure on the builds_dir by enforcing a user driven
        # process to automatically obtain concurrent directories by requesting file locks.
        ff_limit_build_dir = true
        # Indicates how many concurrent build directory can be left on the system. Past
        # this directories will be removed during cleanup (default: 0, unlimited).
        max_build_dir = 10

  - When enabled the base of the builds directory will follow a modified
    structure: ``/<data_dir>/<username>/builds/<projectName>_<projectID>``. This
    attempts to limit the complexity when multiple runners all share the same
    network file system by removing the runner's short token from the path.

Bug & Development Fixes
-----------------------

* Limit simultaneous runs on scheduler tests
  (`!422 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/422>`_)
* Minor code quality corrections
  (`!418 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/418>`_)
* Establish static directory cleanup limit
  (`!424 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/424>`_)

  -  The new ``static_cleanup_limit`` configuration for the static directory
     feature limits the number of folders that can be removed during cleanup
     at any given time. This is meant to avoid situations where a single job
     becomes delayed for an extended period of time removing multiple expired
     directories. The new default for this setting is ``1``.

  - .. code-block:: toml

        [general]
        static_cleanup_limit = 1
