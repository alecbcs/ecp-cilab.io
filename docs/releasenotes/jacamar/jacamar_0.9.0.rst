Jacamar CI v0.9.0
=================

* *Release*: `v0.9.0 <https://gitlab.com/ecp-ci/jacamar-ci/-/releases/v0.9.0>`_
* *Date*: 10/22/2021

.. note::

    There are several key changes to highlight in this release,
    depending on your deployment you may be affected:

    * All RPM files will install binaries and any future files exclusively to
      the ``/opt/jacamar`` folder as opposed to multiple locations.
    * Support for the ``variable_data_dir`` option has been deprecated in favor
      of improvements made to standard ``data_dir`` behaviors and the optional
      RunAs validation script override.
    * The ``downscope = "sudo"`` option is fully supported and associated
      documentation has been added (Thanks `@joe-snyder`).

   Further details along with the remainder of the improvements can be
   found below.

Admin Changes
-------------

* Incorporated better RPM procedures into updated spec file
  (`!256 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/256>`_)

  - To better align with best practices we've focused the installation of
    all Jacamar CI elements into the ``/opt/jacamar`` directory. Any future
    additions will correctly remain within this structure.

  - .. code-block:: console

        $ rpm -qlp jacamar-ci-0.9.0.el7.x86_64.rpm
        /opt/jacamar
        /opt/jacamar/bin
        /opt/jacamar/bin/jacamar
        /opt/jacamar/bin/jacamar-auth

* Expand trusted variables (``$HOME`` and ``$USER``) in ``data_dir``
  (`!272 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/272>`_)

  - Select variables can be referenced by the ``data_dir``, these
    are resolved through information obtained during user-lookup.

  - .. code-block:: toml

        [general]
        data_dir = "/gpfs/ci/$USER"

* Removed support for ``variable_data_dir`` configuration
  (`!268 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/268>`_)

  - Ideally this does not inconvenience any deployments, we believe
    that the changes to ``data_dir`` coupled with the potential use of the
    RunAs validation script offer a stronger solution.

* Properly handle resolution of symlinks in ``data_dir`` - `@tgmachina`
  (`!269 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/269>`_)

  - All symlinks resoled during user scoped directory creation process,
    ensuring enforcement of permissions.

* Bot accounts blocked from runner by default with error message
  (`!266 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/266>`_)

  - Default behaviors can be overridden via configuration if use of
    GitLab bot accounts desired and does not conflict with local user
    authorization processes.

  - .. code-block:: toml

        [auth]
        allow_bot_accounts = true

* Support added for ``tls-ca-file`` configuration for GitLab HTML requests
  (`!265 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/265>`_)

  - Any configured file will be used in constructing HTML requests
    to the associated GitLab instance. Format and behavior mirrors
    that of the
    `GitLab configuration <https://docs.gitlab.com/runner/configuration/advanced-configuration.html#the-runners-section>`_.

  - .. code-block:: toml

        [general]
        tls-ca-file = "/some/file.crt"

* Added ``JWT_ISS`` (issuer) to RunAs validation environment
  (`!276 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/276>`_)
* Batch submission can be affected by ``env_keys`` config
  (`!278 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/278>`_)

  - .. code-block:: toml

        [batch]
        env_keys = ["JOB_CONFIG=/example/test.file"]

  - Equivalent of ``export JOB_CONFIG=/example/test.file && qsub ...``

* Unrecognized configuration keys logged as warnings
  (`!267 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/267>`_)

  - If an unrecognized key is detected in the ``---configuration``
    file this will be logged; however, jobs will be allowed to run.

User Changes
------------

* Correct directory permission provided in related error message
  - `@tgmachina`
  (`!274 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/274>`_)

  - .. code-block:: console

        Error encountered during job: invalid permissions for directory /ecp/<USER>
            (currently drwxr-xr-x, need to be 700), this must be manually addressed
        Error encountered during job: Error executing prepare_exec: exit status 2

* Expand trusted variables (``$HOME`` and ``$USER``) in
  ``CUSTOM_CI_BUILDS_DIR``
  (`!271 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/271>`_)

  - If enabled through administration select variables will be resolved based,
    any other variables or non-absolute paths will still result in errors.

  - .. code-block:: yaml

        variables:
            # Use double $$ to avoid GitLab server resolving variable.
            CUSTOM_CI_BUILDS_DIR: $$HOME/.ci

* Invalid pipeline source error conveyed in job output
  (`!245 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/245>`_)

  - .. image:: files/090/pipeline_source_error.png
        :scale: 60%

Bug & Development Fixes
-----------------------

* Upgraded to Go version `1.17.2`
  (`!273 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/273>`_)
* Added tool versioning via `ASDF <asdf-vm.com/>`_
  (`!259 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/259>`_)
* Sudo args handled as slice for use by ``execve(2)``
  (`!249 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/249>`_)
* Removed all CI Token Broker support
  (`!242 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/242>`_)

  - Support discontinued in favor of the official GitLab functionality for
    `limiting job token access <https://docs.gitlab.com/ee/api/index.html#limit-gitlab-cicd-job-token-access>`_.

* Introduced ``ff_custom_data_dir`` configuration
  (`!271 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/271>`_)

  - This feature flag signals that the entirety of the ``data_dir`` should
    observe a user configured ``CUSTOM_CI_BUILDS_DIR`` variable as opposed
    to just the ``builds_dir``. Behaviors associated with the flag are
    subject to change based upon testing/feedback.

  - .. code-block:: toml

        [general]
        ff_custom_data_dir = true

* Reviewed and updated multiple third-party dependencies
  (`!246 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/246>`_)
* Improved flexibility of validation ``rules`` package while still
  enforcing security requirements
  (`!263 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/263>`_)
* Added OpenSUSE test package during CI
  (`!275 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/275>`_)
* Correctly allow `500` permissions with capabilities
  (`!280 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/280>`_)
* Added `Podman <https://podman.io/>`_ support for local container testing
  (`!255  <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/255>`_,
  `!254 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/254>`_)

  - Container command initiated through the ``Makefile``
    (e.g., ``make test-container``) will default to Podman if found
    installed. Else, or via a ``CONTAINER=docker`` variable, Docker
    will still be utilized.

* JobID logged as integer value
  (`!237 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/237>`_)
* Corrected handling of ``jacamar_path`` config with binary paths
  (`!257 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/257>`_)

  - .. code-block:: toml

        [auth]
        jacamar_path = "/example/jacamar-test-binary"
