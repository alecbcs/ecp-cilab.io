:orphan:

HPC GitLab-Runner 12.4.0~hpc.0.5.1
==================================

* *Release Date*: 12/03/2019
* *Commit*: b502b01c
* *Tag*: `v12.4.0-hpc.0.5.1 <https://gitlab.com/ecp-ci/gitlab-runner/-/tags/v12.4.0-hpc.0.5.1>`_

General Changes
---------------

* Previously a bug could be encountered in cases when a user's global Git
  configuration had set ``[credentials]``. This should now be corrected.

  - .. image:: files/runner/051/credentials-bug.png
        :scale: 50%

  - Similar errors to the above or unexpected timeouts while attempting
    to fetch the repository had been reported.

Admin Changes
-------------

* The changes to account for credentials issues leverages functionality added
  in a later version of Git than some systems may have.
  As such this change now carriers with it a requirement of Git version 2.9+
  be available during all runner job Git interactions. This can be easily
  accounted for via a change to the config.toml file.

  - .. code-block:: toml

        [[runners]]
            name = "example-runner"
            pre_clone_script = '''
                ml use /example/modules/Core && ml git/2.21.0
            '''
            ...

    * The above example assumes the present of LMOD but any module system can
      be used. Note that any commands provided here will be executed by the
      user.

  - For details on the specific Git changes see the
    `associated commit <https://github.com/git/git/commit/24321375cda79f141be72d1a842e930df6f41725>`_
