Report Build Status to GitLab/GitHub
====================================

This example will introduce you to a workflow that can be leveraged
to update a pipeline's build status on a remote GitLab or GitHub
repository within your source project's CI pipeline:

.. image:: files/status/build_status.svg

We will be using the
`GitLab <https://docs.gitlab.com/ee/api/commits.html#post-the-build-status-to-a-commit>`_/`GitHub <https://developer.github.com/v3/repos/statuses/#create-a-commit-status>`_
API to post the build status to a specific commit.
This can easily be accomplished with newer version of ``curl`` and
GitLab's `file CI/CD variable type <https://docs.gitlab.com/ee/ci/variables/#cicd-variable-types>`_.
In this example secrets are stored during the life of the job, limited to a
user's permissions, and will not be visable in either the job log or
to other users on multi-tenant systems.

.. tabs::

    .. tab:: GitLab Source Project

        For GitLab you will require `Pesronal Access Token <https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html>`_
        (or `Project Access Token <https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html>`_
        if targeting a self-hosted instance) with a ``Developer`` role in your
        target project and an ``API`` scope.

        Add this to your source projec as a CI/CD variable:

            .. image:: files/status/gitlab-headers.png
                :scale: 80%

        We strongly advise that the **GITLAB_CURL_HEADERS** variable is
        limited to specific `environment <https://docs.gitlab.com/ee/ci/environments/>`_
        to prevent including it in every job in your pipeline.

        Finally once you've updated your project's CI/CD settings and added
        the necessary script you should incorporate it into your pipeline
        (the ``.gitlab-ci.yml`` file). The example jobs/stages below are envisioned
        as additions to already existing stages using the
        `.pre and .post <https://docs.gitlab.com/ee/ci/yaml/#pre-and-post>`_
        functionality.

        .. literalinclude:: ../../source/build-status-gitlab/gitlab.gitlab-ci.yml
            :language: yaml
            :linenos:

    .. tab:: GitHub Source Project

        For GitHub you will require an
        `access token <https://docs.github.com/en/github/authenticating-to-github/creating-a-personal-access-token>`_.
        with the ``repo:status`` scope.

        Add this to your source projec as a CI/CD variable:

            .. image:: files/status/github-headers.png
                :scale: 80%

        We strongly advise that the **GITHUB_CURL_HEADERS** variable is
        limited to specific `environment <https://docs.gitlab.com/ee/ci/environments/>`_
        to prevent including it in every job in your pipeline.

        Finally once you've updated your project's CI/CD settings and added
        the necessary script you should incorporate it into your pipeline
        (the ``.gitlab-ci.yml`` file). The example jobs/stages below are envisioned
        as additions to already existing stages using the
        `.pre and .post <https://docs.gitlab.com/ee/ci/yaml/#pre-and-post>`_
        functionality.

        .. literalinclude:: ../../source/build-status-gitlab/github.gitlab-ci.yml
            :language: yaml
            :linenos:

After completion trigger a pipeline from the target project and review
the results on your source project:

.. tabs::

    .. tab:: GitLab Project Example

        .. image:: files/status/gitlab-results.png

    .. tab:: GitHub Project Example

        .. image:: files/status/github-results.png
            :scale: 40%

Note that in either case you will need to ensure that the GitLab CI project
pipelines are publicly available or that necessary reviewers have access
to the instance for the link (``CI_PIPELINE_URL``) to function. Else they
will only be able to observe the status with not additioanl context.

Python Scripts
--------------

If using an older deployment with Curl prior `v7.55` it is advisable
to utlize a Python script on multi-tenant resources:

.. tabs::

    .. tab:: GitLab Source Project

        .. literalinclude:: ../../source/build-status-gitlab/gitlab-status.py
            :language: python
            :linenos:

        In the above script we identify all variables from the environment.
        You may recognize that we have used several
        `predefined variables <https://docs.gitlab.com/ee/ci/variables/predefined_variables.html>`_
        that are provided to each CI job as well as several of our own:

        * **BUILDSTATUS_PROJECT**: The upstream project ID.
        * **BUILDSTATUS_APIURL**: The sites API url (e.g. https://gitlab.com/api/v4)
        * **BUILDSTATUS_TOKEN**: An ``api`` scoped
          `personal access token <https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html>`_
          stored in the source GitLab as a
          `masked CI/CD variable <https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project>`_.
        * **BUILDSTATUS_JOB**: Name of the job as it will appear on the source project.

    .. tab:: GitHub Source Project

        .. literalinclude:: ../../source/build-status-gitlab/github-status.py
            :language: python
            :linenos:

        In the above script we identify all variables from the environment.
        You may recognize that we have used several
        `predefined variables <https://docs.gitlab.com/ee/ci/variables/predefined_variables.html>`_
        that are provided to each CI job as well as several of our own:

        * **BUILDSTATUS_OWNER**: Project owner/group as it appears in the repository's url.
        * **BUILDSTATUS_PROJECT**: Project name as it appears in the repository's url.
        * **BUILDSTATUS_APIURL**: The sites API url (e.g. https://api.github.com)
        * **BUILDSTATUS_TOKEN**: A  ``repo:status`` scoped
          `access token <https://docs.github.com/en/github/authenticating-to-github/creating-a-personal-access-token>`_
          stored in the source GitLab as a
          `masked CI/CD variable <https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project>`_.
        * **BUILDSTATUS_JOB**:  Name of the job as it will appear on the source project.
