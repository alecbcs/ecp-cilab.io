Multiple GitLab Project CI Structure
====================================

.. _rules: https://docs.gitlab.com/ee/ci/yaml/#rules

When using GitLab's `repository mirroring <https://docs.gitlab.com/ee/user/project/repository/repository_mirroring.html>`_
functionality you will undoubtedly end up in a case where you will be required
to manage vastly different pipelines depending on the instance. These examples
are meant to provide guidance on some advised methods to handle such a case.

.. note::
  Even with similar workflows, because it is unlikely that two instances uses
  the same runner tags, trying to support two instances quickly becomes
  impossible without the following tricks. (As of April 2021, Gitlab does not
  allow conditional tags ``runner1 OR runner2``).

Single YAML File
----------------

It is possible to manage your pipelines for multiple GitLab instances
through a single ``.gitlab-ci.yml`` file. This can be seen to
great success implemented by the **GAMESS** team and their use of
GitLab rules_ and examining the ``CI_SERVER_HOST``
`predefined variable <https://docs.gitlab.com/ee/ci/variables/predefined_variables.html>`_.

    .. code-block:: yaml

      global-job:
        script:
          - make test

      .rules-siteA
        rules:
        - if: '$CI_SERVER_HOST != "gitlab.siteA.example.com"'
          when: never

      siteA-job:
        extends: .rules-siteA
        script:
          - make test-siteA

      .rules-siteB
        rules:
        - if: '$CI_SERVER_HOST != "gitlab.siteB.example.com"'
          when: never

      siteB-job:
        extends: .rules-siteB
        script:
          - make test-siteB

As you may imagine from reading the example any jobs with the rules_
defined will only run when CI is triggered from the associated server.

.. note::

    We purposefully wrote the rule so that we exclude jobs (``when: never``)
    rather than include them. This is because,
    "Rules are evaluated in order until a match is found".

Multiple YAML Files
-------------------

Maintaining a series of rules_ to enforce behaviors on different servers
may not be supportable. In this case it may be easier to manage
completely separate pipeline files for each GitLab instance. This is achievable
since the CI configuration file (default: ``.gitlab-ci.yml``) is
configurable on a per project basis. To do so simply navigate to **Settings**
--> **CI/CD** --> **General pipelines**:

.. image:: ../tutorial/files/gitlab_ci_timeout.png
    :scale: 75%

By defining a new **Custom CI configuration path** on each GitLab instance
we can manage facility specific configurations. Looking at the contents
of an example project:

    .. code-block:: console

        .gitlab-ci.yml         # Configuration for GitLab.com.
        siteA.gitlab-ci.yml    # Configuration for SiteA's GitLab instance.
        siteB.gitlab-ci.yml    # Configuration for SiteB's GitLab instance.
