Welcome to the ECP CI Documentation
===================================

.. _custom executor: https://docs.gitlab.com/runner/executors/custom.html
.. _GitLab: https://about.gitlab.com

The `Exascale Compute Project <https://www.exascaleproject.org>`_
CI effort is focused on empowering software projects with new models
for testing by enabling access to development HPC resources within their
CI/CD workflows. While also preserving a team's existing development
processes and collaborating with facilities to ensure both the security, as
well as reliability, of all testing capabilities.

.. toctree::
   :caption: Contents
   :maxdepth: 2

   docs/ci-users.rst
   docs/admin.rst

If you are wondering
`"What is Continuous Integration (CI) testing" <https://bssw.io/items/what-is-continuous-integration-testing>`_,
then we encourage you to explore the related articles
from `Better Scientific Software (BSSw) <https://bssw.io/>`_.

Enhancements
------------

Supporting continuous integration on HPC resources necessitated a number
of targeted software enhancements, new services, and the establishment of
improved workflow capabilities:

* **Advanced Workflows**: An ongoing collaborative effort between not
  just facility administrators but project teams to established documented
  workflows and best practices.
* **Batch Executors**: Run user defined scripts at the job level by submitting
  to the underlying scheduling system.
* **Custom Executor Compatibility**: Upstream GitLab has added support for
  the `custom executor`_
  which allows for the implementation of administratively defined application
  to realize runner functionality. All future realizations of runner side
  enhancements will be completed within this exciting model.
* **Downscoping Permissions**: Targeting the validated user responsible for
  triggering the CI pipeline and dropping permissions prior to job execution
  to that of their system local account.

The documentation found on this site elaborates on these
enhancements for both individuals who wish to leverage CI resources
as well as administrators planning to deploy these tools. As many of
the enhancements are still under development, we welcome and appreciate
any feedback/questions you may have.

CI Use Models
-------------

Facilities, as well as users, can leverage the ECP CI enhancements
realized in the runner via the `custom executor`_ in a manner that supports
the evolving needs of projects. Due to the flexibility afforded by
GitLab_ both in source code management, as well as CI, via a range of
supported `executor <https://docs.gitlab.com/runner/executors/>`_
and project level customization.

Sites can deploy an environment consisting of a local server, ECP
enhanced executor, along with any additional resources they wish to
support. The local model can be used to leverage resources unavailable
outside of the facility while still benefiting from publicly available
development, documentation, and pre-established workflows.

We are not intending the ECP CI effort to completely replace any existing
CI workflows. Instead, we want these tools to benefit from flexibility offered
to CI/CD pipelines both in configuration and runner deployment. Ideally these
CI models will create added value when leveraged as part of any comprehensive
testing strategy.

Open Source Projects
--------------------

In order to realize the goals of the ECP CI effort we maintain several
open source projects:

* Jacamar CI: `gitlab.com/ecp-ci/jacamar-ci <https://gitlab.com/ecp-ci/jacamar-ci>`_

    - HPC focused CI/CD driver for the GitLab custom executor.

*  Job Context Validation: `gitlab.com/ecp-ci/gljobctx-go <https://gitlab.com/ecp-ci/gljobctx-go>`_

    - Workflow for validating and providing claims from a ``CI_JOB_JWT``.

* ECP-CI.GitLab.io: `gitlab.com/ecp-ci/ecp-ci.gitlab.io <https://gitlab.com/ecp-ci/ecp-ci.gitlab.io>`_

    - Documentations and Pages for CI efforts.
